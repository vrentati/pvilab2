$(document).ready(function () {
    $(document).on('click', '[data-open-delete-user-modal]', function () {
        openDeleteUserModal($(this));
    });

    function openDeleteUserModal(button) {
        var row = button.closest('tr');
        var modal = $('#deleteUserModal');
        var id = row.find('td:eq(0) input[type="checkbox"]').attr('data-user-id');
        var firstName = row.find('td:eq(2)').text();
        $('#deleteUserMessage', modal).text(`Are you sure you want to delete user ${firstName}?`);
        modal.data('user-id', id);
        selectedRow = row;

        var modalInstance = new bootstrap.Modal(modal);
        modalInstance.show();
    }


    $('#confirmDeleteBtn').click(function () {
        console.log("Button clicked");
        confirmDelete();
    });


    function confirmDelete() {
        var modal = $('#deleteUserModal');
        var id = modal.data('user-id');
        var table = selectedRow.closest('tbody');
        selectedRow.remove();

        var newModal = bootstrap.Modal.getInstance(modal);
        newModal.hide();
    }

    var editingRowIndex = -1;

    $('#saveStudentBtn').click(function () {
        var $studentTable = $('#studentTable');
        var $groupField = $('#group');
        var $firstNameField = $('#firstName');
        var $lastNameField = $('#lastName');
        var $genderField = $('#gender');
        var $birthdayField = $('#birthday');
        var $statusCheckbox = $('#status');

        var id = $('#studentId').val();
        var formData = {
            id: id,
            group: $groupField.val(),
            firstName: $firstNameField.val(),
            lastName: $lastNameField.val(),
            birthday: $birthdayField.val(),
            gender: $genderField.val(),
            status: $statusCheckbox.is(':checked')
        };

        var birthday = formData.birthday.split('-').reverse().join('-');

        if (editingRowIndex !== -1) {
            var $row = $studentTable.find('tbody tr').eq(editingRowIndex);
            $row.find('td:eq(1)').text(formData.group);
            $row.find('td:eq(2)').text(formData.firstName + ' ' + formData.lastName);
            $row.find('td:eq(3)').text(formData.gender[0]);
            $row.find('td:eq(4)').text(birthday);
            $row.find('td:eq(5) input[type="checkbox"]').prop('checked', formData.status);
        } else {
            $studentTable.find('tbody').append('<tr>' +
                '<td><input class="custom-checkbox2" type="checkbox" data-user-id="' + id + '"></td>' +
                '<td>' + formData.group + '</td>' +
                '<td>' + formData.firstName + ' ' + formData.lastName + '</td>' +
                '<td>' + formData.gender[0].toUpperCase() + '</td>' +
                '<td>' + birthday + '</td>' +
                '<td><div class="circle" ' + (formData.status ? 'active' : '') + '></td>' +
                '<td><button class="icon-btn btn btn-outline-secondary btn-sm" data-open-delete-user-modal>' +
                '<i class="bi bi-x"></i></button>' +
                ' <button class="icon-btn btn btn-outline-secondary btn-sm editStudentBtn" data-bs-toggle="modal" data-bs-target="#studentModal"><i class="bi bi-pencil"></i></button></td>' +
                '</tr>');
        }

        $('#studentModal').modal('hide');

        editingRowIndex = -1;

        $('#group, #firstName, #lastName, #gender, #birthday').val('');
        $('#status').prop('checked', false);
    });

    $('#studentModal').on('hidden.bs.modal', function () {
        $('#group, #firstName, #lastName, #gender, #birthday').val('');
        $('#status').prop('checked', false);
    });

    $('#cancelBtn').click(function () {
        $('#group, #firstName, #lastName, #gender, #birthday').val('');
        $('#status').prop('checked', false);
    });

    $(document).on('click', '.editStudentBtn', function () {
        editingRowIndex = $(this).closest('tr').index();
        var $row = $('#studentTable tbody tr').eq(editingRowIndex);
        var id = $row.find('td:eq(0) input[type="checkbox"]').attr('data-user-id');
        var group = $row.find('td:eq(1)').text();
        var fullName = $row.find('td:eq(2)').text().split(' ');
        var gender = $row.find('td:eq(3)').text();
        var birthday = $row.find('td:eq(4)').text();
        var status = $row.find('td:eq(5) input[type="checkbox"]').prop('checked');

        $('#studentId').val(id);
        $('#group').val(group);
        $('#firstName').val(fullName[0]);
        $('#lastName').val(fullName[1]);
        if (gender[0] === 'M') gender = 'Mail';
        if (gender[0] === 'F') gender = 'Femail';
        $('#gender').val(gender);
        $('#birthday').val(birthday);
        $('#status').prop('checked', status);
        $('#studentModal').modal('show');
    });
});
